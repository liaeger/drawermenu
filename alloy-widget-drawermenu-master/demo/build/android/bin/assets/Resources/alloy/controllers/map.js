function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "map";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.labelWindow = Ti.UI.createWindow({
        id: "labelWindow"
    });
    $.__views.labelWindow && $.addTopLevelView($.__views.labelWindow);
    $.__views.msgLabel = Ti.UI.createLabel({
        text: "Testing",
        id: "msgLabel"
    });
    $.__views.labelWindow.add($.__views.msgLabel);
    $.__views.image = Ti.UI.createImageView({
        id: "image",
        image: "/cmhmap.png"
    });
    $.__views.labelWindow.add($.__views.image);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;