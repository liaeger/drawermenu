function Controller() {
    function openMap() {
        var Map = Alloy.createController("map").getView();
        Map.open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menuview";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.menuView = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#FF9933",
        id: "menuView"
    });
    $.__views.menuView && $.addTopLevelView($.__views.menuView);
    $.__views.menuTopBar = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: "60dp",
        backgroundColor: "#C5C5C5",
        layout: "horizontal",
        id: "menuTopBar"
    });
    $.__views.menuView.add($.__views.menuTopBar);
    var __alloyId5 = [];
    $.__views.row1 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row1"
    });
    __alloyId5.push($.__views.row1);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row1.add($.__views.rowContainer);
    $.__views.rowHome = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/house.png",
        id: "rowHome"
    });
    $.__views.rowContainer.add($.__views.rowHome);
    $.__views.homeLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Home",
        id: "homeLabel"
    });
    $.__views.rowContainer.add($.__views.homeLabel);
    $.__views.row2 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row2"
    });
    __alloyId5.push($.__views.row2);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row2.add($.__views.rowContainer);
    $.__views.rowSettings = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/19-gear.png",
        id: "rowSettings"
    });
    $.__views.rowContainer.add($.__views.rowSettings);
    $.__views.settingsLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Configurations",
        id: "settingsLabel"
    });
    $.__views.rowContainer.add($.__views.settingsLabel);
    $.__views.row3 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row3"
    });
    __alloyId5.push($.__views.row3);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row3.add($.__views.rowContainer);
    $.__views.rowDolphin = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/dolphin.png",
        id: "rowDolphin"
    });
    $.__views.rowContainer.add($.__views.rowDolphin);
    $.__views.dolphinLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Dolphin",
        id: "dolphinLabel"
    });
    $.__views.rowContainer.add($.__views.dolphinLabel);
    $.__views.row4 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row4"
    });
    __alloyId5.push($.__views.row4);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row4.add($.__views.rowContainer);
    $.__views.rowMap = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/map.png",
        id: "rowMap"
    });
    $.__views.rowContainer.add($.__views.rowMap);
    $.__views.mapLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Map",
        id: "mapLabel"
    });
    $.__views.rowContainer.add($.__views.mapLabel);
    openMap ? $.__views.mapLabel.addEventListener("click", openMap) : __defers["$.__views.mapLabel!click!openMap"] = true;
    $.__views.row5 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row5"
    });
    __alloyId5.push($.__views.row5);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row5.add($.__views.rowContainer);
    $.__views.rowCalendar = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/calendar.png",
        id: "rowCalendar"
    });
    $.__views.rowContainer.add($.__views.rowCalendar);
    $.__views.CalendarLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Calendar",
        id: "CalendarLabel"
    });
    $.__views.rowContainer.add($.__views.CalendarLabel);
    $.__views.row6 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row6"
    });
    __alloyId5.push($.__views.row6);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row6.add($.__views.rowContainer);
    $.__views.exhibitImage = Ti.UI.createImageView({
        width: "30dp",
        height: "30dp",
        left: "2",
        id: "exhibitImage",
        image: "/exhibit.png"
    });
    $.__views.rowContainer.add($.__views.exhibitImage);
    $.__views.exhibitLabel = Ti.UI.createLabel({
        top: 7,
        left: "2",
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Exhibits",
        id: "exhibitLabel"
    });
    $.__views.rowContainer.add($.__views.exhibitLabel);
    $.__views.row7 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row7"
    });
    __alloyId5.push($.__views.row7);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row7.add($.__views.rowContainer);
    $.__views.rowHome = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/house.png",
        id: "rowHome"
    });
    $.__views.rowContainer.add($.__views.rowHome);
    $.__views.homeLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Home",
        id: "homeLabel"
    });
    $.__views.rowContainer.add($.__views.homeLabel);
    $.__views.row8 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row8"
    });
    __alloyId5.push($.__views.row8);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row8.add($.__views.rowContainer);
    $.__views.rowSettings = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/19-gear.png",
        id: "rowSettings"
    });
    $.__views.rowContainer.add($.__views.rowSettings);
    $.__views.settingsLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Configurations",
        id: "settingsLabel"
    });
    $.__views.rowContainer.add($.__views.settingsLabel);
    $.__views.row9 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row9"
    });
    __alloyId5.push($.__views.row9);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row9.add($.__views.rowContainer);
    $.__views.rowDolphin = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/dolphin.png",
        id: "rowDolphin"
    });
    $.__views.rowContainer.add($.__views.rowDolphin);
    $.__views.dolphinLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Dolphin",
        id: "dolphinLabel"
    });
    $.__views.rowContainer.add($.__views.dolphinLabel);
    $.__views.row10 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row10"
    });
    __alloyId5.push($.__views.row10);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row10.add($.__views.rowContainer);
    $.__views.rowMap = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/map.png",
        id: "rowMap"
    });
    $.__views.rowContainer.add($.__views.rowMap);
    $.__views.mapLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Map",
        id: "mapLabel"
    });
    $.__views.rowContainer.add($.__views.mapLabel);
    $.__views.row11 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row11"
    });
    __alloyId5.push($.__views.row11);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row11.add($.__views.rowContainer);
    $.__views.rowCalendar = Ti.UI.createView({
        left: 5,
        top: 7,
        width: "20dp",
        height: "20dp",
        backgroundImage: "/calendar.png",
        id: "rowCalendar"
    });
    $.__views.rowContainer.add($.__views.rowCalendar);
    $.__views.CalendarLabel = Ti.UI.createLabel({
        top: 7,
        left: 10,
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Calendar",
        id: "CalendarLabel"
    });
    $.__views.rowContainer.add($.__views.CalendarLabel);
    $.__views.row12 = Ti.UI.createTableViewRow({
        height: "50dp",
        id: "row12"
    });
    __alloyId5.push($.__views.row12);
    $.__views.rowContainer = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "rowContainer"
    });
    $.__views.row12.add($.__views.rowContainer);
    $.__views.exhibitImage = Ti.UI.createImageView({
        width: "30dp",
        height: "30dp",
        left: "2",
        id: "exhibitImage",
        image: "/exhibit.png"
    });
    $.__views.rowContainer.add($.__views.exhibitImage);
    $.__views.exhibitLabel = Ti.UI.createLabel({
        top: 7,
        left: "2",
        height: "20dp",
        font: {
            fontSize: "15dp"
        },
        color: "#000",
        text: "Exhibits",
        id: "exhibitLabel"
    });
    $.__views.rowContainer.add($.__views.exhibitLabel);
    $.__views.menuTable = Ti.UI.createTableView({
        separatorStyle: "NONE",
        separatorColor: "transparent",
        backgroundColor: "#F2F2F2",
        data: __alloyId5,
        id: "menuTable"
    });
    $.__views.menuView.add($.__views.menuTable);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    __defers["$.__views.mapLabel!click!openMap"] && $.__views.mapLabel.addEventListener("click", openMap);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;