function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "mainview";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.mainView = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#FFFFFF",
        id: "mainView"
    });
    $.__views.mainView && $.addTopLevelView($.__views.mainView);
    $.__views.mainTopBar = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: "60dp",
        backgroundColor: "#FF6600",
        layout: "horizontal",
        id: "mainTopBar"
    });
    $.__views.mainView.add($.__views.mainTopBar);
    $.__views.menuButton = Ti.UI.createView({
        width: "60dp",
        height: "60dp",
        backgroundColor: "#FF9933",
        id: "menuButton"
    });
    $.__views.mainTopBar.add($.__views.menuButton);
    $.__views.maintitleLabel = Ti.UI.createLabel({
        left: "200",
        font: {
            fontSize: "20dp",
            fontWeight: "bold"
        },
        color: "white",
        text: "Homepage",
        id: "maintitleLabel"
    });
    $.__views.mainTopBar.add($.__views.maintitleLabel);
    var __alloyId3 = [];
    $.__views.__alloyId4 = Ti.UI.createTableViewRow({
        id: "__alloyId4"
    });
    __alloyId3.push($.__views.__alloyId4);
    $.__views.eventsLabel = Ti.UI.createLabel({
        left: "10",
        top: "20",
        font: {
            fontSize: "20dp",
            fontWeight: "bold"
        },
        color: "black",
        text: "Events",
        id: "eventsLabel"
    });
    $.__views.__alloyId4.add($.__views.eventsLabel);
    $.__views.image = Ti.UI.createImageView({
        height: "100dp",
        width: "320",
        top: "60",
        id: "image",
        image: "/event1.jpg"
    });
    $.__views.__alloyId4.add($.__views.image);
    $.__views.__alloyId2 = Ti.UI.createTableView({
        data: __alloyId3,
        id: "__alloyId2"
    });
    $.__views.mainView.add($.__views.__alloyId2);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;